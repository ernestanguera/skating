# House Of Skateboarding
##Projecte web
- Integrants: Ernest Anguera i Isaac León.
- Tema del web: Skateboarding.
- Títol del web: House of Skateboarding
- Esquema del lloc web: 
    1. Pàgina principal (actuarà com a index):
        1. Què és l’Skateboarding?
        2. Una mica d’història.
    2. Estils d’Skateboarding:
        1. [Web amb els diferents estils.](https://bextremeboards.com/blog/estilos-de-skate-cuales-existen/) (Font d’on obtindrem la informació).
    3. Tipus d’Skate (board):
        1. Descripció dels diferents tipus d’Skate (long, penny, etc.).
    4. Tricks:
        1. Descripció de diferents tricks (trucs) a poder fer amb l’Skate.
